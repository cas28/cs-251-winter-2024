%%%%%%%%%%%%%%%%%
%% style stuff %%
%%%%%%%%%%%%%%%%%

% parskip: add \parskip between each paragraph
% scrartcl: KOMA-Script article class
\documentclass[parskip]{scrartcl}

% page margins
\usepackage[margin=1in]{geometry}

% text encoding
\usepackage[T1]{fontenc}

% font colors
\usepackage{xcolor}

% auto-sizing columns
\usepackage{varwidth}

% padding in boxes
\usepackage{adjustbox}

% no extra space between list items
\usepackage{enumitem}

% scale elements with \scalebox
% rotate elements with \rotatebox
\usepackage{graphicx}

% landscape pages
\usepackage{lscape}

% hyperlinks
% hidelinks: don't style hyperlinks
% linktoc=all: hyperlink both the section titles and the page numbers in TOC
\usepackage[hidelinks,linktoc=all]{hyperref}
\usepackage{xurl}

% labels for nested numbered lists
\renewcommand{\labelenumii}{\arabic{enumi}.\arabic{enumii}}
\renewcommand{\labelenumiii}{\arabic{enumi}.\arabic{enumii}.\arabic{enumiii}}
\renewcommand{\labelenumiv}{\arabic{enumi}.\arabic{enumii}.\arabic{enumiii}.\arabic{enumiv}}

% spaces that disappear before punctuation marks
\usepackage{xspace}

% thick horizontal lines
\usepackage{boldline}

% thick horizontal line
\newcommand{\thickhline}{\hlineB{4}}
% thick vertical line in tabular environment
\newcolumntype{?}{!{\vrule width 1.5pt}}

% table cells spanning more than one row
\usepackage{multirow}

% thick horizontal line spanning only some columns
% https://tex.stackexchange.com/a/173535
\makeatletter
\def\Cline#1#2{\@Cline#1#2\@nil}
\def\@Cline#1-#2#3\@nil{%
  \omit
  \@multicnt#1%
  \advance\@multispan\m@ne
  \ifnum\@multicnt=\@ne\@firstofone{&\omit}\fi
  \@multicnt#2%
  \advance\@multicnt-#1%
  \advance\@multispan\@ne
  \leaders\hrule\@height#3\hfill
  \cr}
\makeatother
\newcommand{\thickcline}[1]{\Cline{#1}{1.5pt}}

% circles around numbers/letters/symbols
\usepackage{circledsteps}

% multi-column enumerate/itemize
\usepackage{multicol}


%%%%%%%%%%%%%%%%%
%% logic stuff %%
%%%%%%%%%%%%%%%%%

% math symbols
\usepackage{MnSymbol}

\usepackage{amsfonts}

% syntax trees
\usepackage{forest}

% proof trees
\usepackage{bussproofs}


%%%%%%%%%%%%%%%%%%%%%
%% semantic styles %%
%%%%%%%%%%%%%%%%%%%%%

% styles
\newcommand{\vocabstyle}[1]{\emph{#1}}
\newcommand{\vocab}[1]{\vocabstyle{\gls{#1}}}
\newcommand{\vocabs}[1]{\vocabstyle{\glspl{#1}}}
\newcommand{\Vocab}[1]{\vocabstyle{\Gls{#1}}}
\newcommand{\Vocabs}[1]{\vocabstyle{\Glspl{#1}}}
\newcommand{\vocablink}[2]{\glslink{#1}{\vocabstyle{#2}}}
\newcommand{\insist}[1]{\textbf{#1}}
\newcommand{\mono}[1]{\texttt{#1}}
\newcommand{\linkstyle}[1]{{\color{blue} \underline{#1}}}
\newcommand{\urlstyled}[1]{\linkstyle{\url{#1}}}
\newcommand{\hrefstyled}[2]{\linkstyle{\href{#1}{#2}}}
\newcommand{\metavar}[1]{\fbox{\ensuremath{#1}}}
\newcommand{\varmetavar}[1]{\fcolorbox{black}{black!15}{\ensuremath{#1}}}
\newcommand{\rulename}[1]{\textsc{#1}}
\newcommand{\assumption}[1]{\ensuremath{\stackrel{\scalebox{1}{#1}}{\vdots}}}
\newcommand{\var}[1]{\ensuremath{\overline{#1}}}

% symbols
\newcommand{\position}{\mono{\_}}
\newcommand{\entails}{\ensuremath{\vdash}}
\newcommand{\equals}{\ensuremath{=}}
\newcommand{\identical}{\ensuremath{\triangleq}}
\newcommand{\subsumedby}{\ensuremath{\sqsubseteq}}
\newcommand{\nsubsumedby}{\ensuremath{\nsqsubseteq}}
\newcommand{\mightsubsumedby}{\ensuremath{\overset{?}{\sqsubseteq}}}
\newcommand{\defequals}{\ensuremath{:=}}
\newcommand{\alphaequiv}{\ensuremath{\overset{\alpha}{=}}}
\newcommand{\unifyequals}{\ensuremath{\doteq}}
\newcommand{\mightequals}{\ensuremath{\overset{\boldsymbol{\cdot}}{=}}}
\newcommand{\mightle}{\ensuremath{\overset{?}{\le}}}
\newcommand{\cons}{\ensuremath{\squaredots}}
\newcommand*{\threesim}{\mathrel{\vcenter{\offinterlineskip \hbox{$\sim$}\vskip-.35ex\hbox{$\sim$}\vskip-.35ex\hbox{$\sim$}}}} % from Comprehensive LaTeX Symbol List
\newcommand{\subsume}{\ensuremath{\sqsubseteq}}
\newcommand{\negate}{\ensuremath{\neg}\mkern2mu}


%%%%%%%%%%%%%%%%%%%%
%% glossary stuff %%
%%%%%%%%%%%%%%%%%%%%

% needs to be below hyperref import
% acronyms: support for acronym entries
% nopostdot: don't put extra periods after acronym entries
% numberedsection: include section number in TOC glossary link
% section: change sectioning command so acronyms and glossary can be on the same page
% style=super: no extra whitespace
% toc: include glossary in TOC
% xindy: better indexing
\usepackage[acronyms,nogroupskip,nopostdot,section,style=super,toc,xindy]{glossaries}

% on first use of each acronym, print the long version followed by the acronym
\setacronymstyle{long-short}

% all acronyms are manually-defined
\newacronym{cpl}{CPL}{classical propositional logic}
\newacronym{ipl}{IPL}{intuitionistic propositional logic}
\newacronym{lem}{LEM}{law of the excluded middle}
\newacronym{stlc}{STLC}{simply-typed lambda calculus}
\newacronym{ast}{AST}{abstract syntax tree}

% manually-defined glossary terms
\longnewglossaryentry{alpha-equivalent}{name=$\alpha$-equivalent}{}
\longnewglossaryentry{definitional-equality}{name=definitional equality,plural=definitional equalities}{}
\longnewglossaryentry{first-order}{name=first-order}{}
\longnewglossaryentry{proof-relevance}{name=proof-relevance}{}
\longnewglossaryentry{rule-of-inference}{name=rule of inference,plural=rules of inference}{}
\longnewglossaryentry{short-circuiting}{name=short-circuiting}{}
\longnewglossaryentry{single-sided}{name=single-sided}{}
\longnewglossaryentry{tautology}{name=tautology,plural=tautologies}{}
\longnewglossaryentry{truth-preserving}{name=truth-preserving}{}
\longnewglossaryentry{unify-with}{name=unify with,plural=unifies with}{}
\longnewglossaryentry{value-preserving}{name=value-preserving}{}
\longnewglossaryentry{well-formed-context}{name=well-formed context}{}
\longnewglossaryentry{well-formedness}{name=well-formedness}{}

% manually-defined glossary symbols
\longnewglossaryentry{sym-latex}{name=\LaTeX, sort=0latex}{}
\longnewglossaryentry{sym-substitute}{name=\position[\position], sort=0substitute}{}


%%%%%%%%%%%%
%% frames %%
%%%%%%%%%%%%

% CSS-like border/padding/margin
\usepackage{longfbox}

% inline frames around block things
\newfboxstyle{inline}{margin=0.5em,padding=0.5em,text-align=left,border-style=none}
\newcommand{\inline}[1]{\lfbox[inline]{#1}}

% frames around warnings
\newfboxstyle{warning}{margin=0.5em,padding=0.5em,text-align=left,border-color=red}
\newcommand{\warning}[1]{\lfbox[warning]{\begin{varwidth}{\textwidth}#1\end{varwidth}}}
\newcommand{\longwarning}[1]{\begin{longfbox}[warning,breakable=true]#1\end{longfbox}}

% frames around examples
\newfboxstyle{example}{margin=0.5em,padding=0.5em,text-align=left}
\newcommand{\example}[1]{\lfbox[example]{\begin{varwidth}{\textwidth}#1\end{varwidth}}}
\newcommand{\longexample}[1]{\begin{longfbox}[example,breakable=true]#1\end{longfbox}}

% frames around in-lecture demonstrations
\newfboxstyle{demo}{margin=0.5em,padding=0.5em,text-align=left,border-style=dashed}
\newcommand{\demo}[1]{\lfbox[demo]{\begin{varwidth}{\textwidth}#1\end{varwidth}}}
\newcommand{\longdemo}[1]{\begin{longfbox}[demo,breakable=true]#1\end{longfbox}}


%%%%%%%%%%%%%%%%%
%% title stuff %%
%%%%%%%%%%%%%%%%%
\subject{\vspace{-1cm}CS 251 Discrete Structures II}
\author{Katie Casamento}
\date{Winter 2024}
\publishers{Portland State University}

\newcommand{\indextitle}[1]{\title{\jobname: #1}}
