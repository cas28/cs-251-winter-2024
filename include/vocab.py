#!/usr/bin/python3

from re import findall
from sys import argv

SYMBOL = "sym-"

text = open(argv[1], "r").read()

alreadyDefined = set(findall(r"glossaryentry{(.*?)}", text + open("include/header.tex", "r").read()))
used = set(findall(r"\\(?:[Vv]ocabs?|vocablink){(.*?)}", text))

for term in used:
  if term not in alreadyDefined:
    if term.startswith(SYMBOL):
      print(
        "\\longnewglossaryentry{" + term +
        "}{name=\\ensuremath{\\" + term[len(SYMBOL):] +
        "}, sort=0" + term[len(SYMBOL):] + "}{}"
      )
    else:
      print(
        "\\longnewglossaryentry{" + term +
        "}{name=" + term.replace('-', ' ') +
        "}{}"
      )
