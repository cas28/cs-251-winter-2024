\input{include/header.tex}
\input{include/theories/bool.tex}

% auto-generated glossary terms
\input{notes/2.glt}

\indextitle{Expression structure}
\subtitle{What are rules, anyway?}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Introduction}

  In an algebra course long ago, you learned that $x + y = y + x$ is a law of arithmetic, and you learned to use that fact to conclude things like $1 + 2 = 2 + 1$ or $(a \times b^2) + c = c + (a \times b^2)$.

  But wait --- if we can use $x + y = y + x$ to conclude $1 + 2 = 2 + 1$, why \insist{can't} we use $x + y = y + x$ to conclude $1 + 2 = 2 + 2$?

  In these notes we will explore a precise answer to this deceptively simple question of ``how rules work''. Several terms and concepts will probably be new to you, but they will all connect back to your experience with the basic rules of arithmetic.


\section{Abstract syntax trees}

  An \vocablink{abstract-syntax-tree}{\acrlong{ast}} (AST) is a way to represent the precise order of operations in an expression. ASTs are a very common form of data structure in the study of logic and programming languages, because we often have a need to be precise about the order of operations in our expressions.

  \subsection{Definition}

    In an AST diagram, the \insist{last} operation in the order of operations is the \insist{root} of the tree, which is traditionally drawn at the top of the diagram. Here's an example for an arithmetic expression using familiar operators:

    \begin{center}
      $-a \times b + c \times -(d + e)$

      \begin{forest}
        [$+$
          [$\times$
            [$-$
              [$a$]
            ]
            [$b$]
          ]
          [$\times$
            [$c$]
            [$-$
              [$+$
                [$d$]
                [$e$]
              ]
            ]
          ]
        ]
      \end{forest}
    \end{center}

    Note how the parentheses from the original expression are not written in the AST, because the \insist{nesting structure} of the AST already indicates that the $c + d$ part ``happens before'' the negation above it.

    In our standard Boolean logic order of operations, negation ``happens first'', then conjunction, disjunction, implication, and finally equivalence. Consider this Boolean expression and its AST:

    \begin{center}
      $\neg (P \conj \neg Q \disj R) \equ R \impl P$

      \begin{forest}
        [$\equ$
          [$\neg$
            [$\disj$
              [$\conj$
                [$P$]
                [$\neg$
                  [$Q$]
                ]
              ]
              [$R$]
            ]
          ]
          [$\impl$
            [$R$]
            [$P$]
          ]
        ]
      \end{forest}
    \end{center}

    Notice how the structure of the diagram indicates the order of operations in the expression: the $\equ$ operator will ``happen last''.

  \subsection{Terminology}

    When we describe an expression as ``an equivalence'', we are generally indicating that the \insist{last} operator in the expression is the $\equ$ operator. Similarly, when we describe an expression as ``a conjunction'', ``a disjunction'', ``an implication'', etc., we are generally talking about the \insist{last} operator: the \insist{root} of the AST.

    In contrast, consider this expression:

    \begin{center}
      $(P \equ Q) \disj (Q \equ P)$

      \begin{forest}
        [$\disj$
          [$\equ$
            [$P$]
            [$Q$]
          ]
          [$\equ$
            [$Q$]
            [$P$]
          ]
        ]
      \end{forest}
    \end{center}

    While there are two \vocabs{subexpression} here involving the $\equ$ operator, we would \insist{not} generally say that $(P \equ Q) \disj (Q \equ P)$ ``is an equivalence''; instead we would say that it ``is a disjunction'', because the \insist{root} of its AST is the $\disj$ operator.


\section{Identity}

  Two expressions can only have the same \vocab{abstract-syntax-tree} if they are \insist{written in the exact same way} (ignoring optional parentheses). When we show that two expressions are tautologically equivalent, we are almost always showing an equivalence between two expressions with \insist{different} ASTs.

  Since we might also sometimes use the term ``equal'' to describe two expressions that are equivalent, we need a different term to describe the much \insist{stronger} property of two expressions having the same AST.

  \subsection{Definition}

    When two expressions have the same AST, we say they are \vocab{identical}. The property itself is called \vocab{identity}. In these notes, we will write the ``triangle equals'' symbol \vocab{sym-identical} to represent \vocab{identity}, in order to distinguish it from \vocab{equivalence}.

    When two expressions are identical, they have the exact same ``form'' or ``structure''. When two expressions are equivalent, they have the same \insist{truth value}, but they may still be different in other ways, like when we look at them as ASTs.

    This is why we say that identity is \insist{stronger} than equivalence: we have that $P \identical Q$ implies $P \equ Q$, but not the other way around.

    For example, $\neg \neg P \identical \neg (\neg P)$, because the two expressions have the same AST: they look different because of the optional parentheses on the right, but optional parentheses do not change the meaning of the expression in any way.

    \begin{center}
      \begin{forest}
        [$\neg$
          [$\neg$
            [$P$]
          ]
        ]
      \end{forest}
    \end{center}

    $\neg (P \conj Q)$ and $\neg P \disj \neg Q$ are equivalent but not identical, because $\neg (P \conj Q) \equ (\neg P \conj \neg Q)$ is a tautology, but the two expressions have different ASTs.

    \begin{center}
      \begin{forest}
        [$\neg$
          [$\conj$
            [$P$]
            [$Q$]
          ]
        ]
      \end{forest}
      \quad
      \begin{forest}
        [$\disj$
          [$\neg$
            [$P$]
          ]
          [$\neg$
            [$Q$]
          ]
        ]
      \end{forest}
    \end{center}


\section{Substitutions}

  When we refer to symbols like $A$ and $P$ as \vocabs{variable}, we are suggesting that they might take on different assignments throughout our analysis. For example, we might ask ``what is the truth value of the expression $P \conj \neg Q$ when $P \defequals \true$ and $Q \defequals \true \conj \false$?''

  We will use the \vocab{sym-defequals} symbol for these kinds of ``variable assignments'' to indicate that we are stating these equalities as \insist{definitions} of $P$ and $Q$. Note that \insist{defining} $P \defequals \true$ is very different than \insist{discovering} or \insist{proving} that something is true, which is why we use a different symbol for equalities that we \insist{define}. With this in mind, we will usually pronounce $P \defequals \true$ as ``$P$ is defined to be true'', not ``$P$ equals true''.

  \subsection{Definition}

    The term \vocab{substitution} describes a collection of variable definitions, which must have no overlap (no variable in a single substitution can have more than one definition). For example, we might write the substitution from the question above as $\left[ P \defequals \true,\ Q \defequals \true \conj \false \right]$.

    When we \vocab{apply} a substitution to an expression, we consistently replace every use of the substituted variables with their definitions. We can think of a substitution as a \vocab{function}, leading to one traditional notation for applying a substitution:

    \begin{center}
      $\left[P \defequals \true,\ Q \defequals \true \conj \false\right](P \conj \neg Q) \quad \identical \quad \true \conj \neg (\true \conj \false)$
    \end{center}

    Notice the use of the identity operator here instead of equivalence! When we apply a substitution as a function, the result we get has the \insist{same tree structure} as the original expression, \insist{except} the variables have been replaced. We sometimes use the term ``grafting'' to describe how the ASTs of the variable definitions become subtrees in the output AST.

  \subsection{Special cases}

    In general, it is valid for the definitions in a substitution to have variables on the right side of the $\defequals$ symbol. For example, $\left[P \defequals A,\ Q \defequals A \disj B\right]$ is a valid substitution. We sometimes call this an \vocab{open-substitution}; the opposite is a \vocab{closed-substitution}, with no variables on the right side of any of its definitions.

    It is also valid to apply a substitution to an expression even if some variables from the expression are not defined in the substitution. For example, $\left[P \defequals \true\right](P \conj Q)$ is a valid application with the result $\true \conj Q$. We sometimes call this a \vocab{partial-application}; the opposite is a \vocab{total-application}, where every variable in the expression is given a definition in the substitution.

    While open substitutions and partial applications are meaningful concepts in general, we often focus on the special cases of closed substitutions and total applications. A total application of a closed substitution gives us a \vocab{closed-expression}, an expression with no variables.


\section{Subsumption}

  In a sense, \vocab{equivalence} is the ``weakest equality'' between expressions: it only requires that the two expressions have the same \insist{value}, without requiring anything specific about their \insist{structure}. This is why we can define it straightforwardly ``inside'' our system of truth tables.

  As we move into our study of Boolean algebra, we will start to examine the \insist{structure} of our expressions in much more depth. This is why the concept of \vocab{identity} is important: identity is the ``strongest equality'' between expressions, because it requires that the two expressions have the exact same \insist{structure}. This is why we need to define it ``outside'' our system of truth tables, with the concept of an AST.

  \subsection{Definition}

    In between the extremes of equivalence and identity, we have the concept of \vocab{subsumption}. Very roughly, we say that one expression is \vocab{subsumed-by} another expression when it is possible for them to \insist{become} identical by applying a substitution.

    More precisely, we say $e_1$ is \vocab{subsumed-by} $e_2$ (or $e_2$ subsumes $e_1$) if there is \insist{some} substitution $\sigma$ where $e_1 \identical \sigma(e_2)$. In symbols, we will write $e_1\ \vocab{sym-subsume}\ e_2$ for ``$e_1$ is subsumed by $e_2$''.

    For example, we have $\neg (A \impl B) \conj A \subsumedby P \conj Q$, because with $\sigma \defequals \left[P \defequals \neg (A \impl B),\ Q \defequals A\right]$, we have $\neg (A \impl B) \conj A \identical \sigma(P \conj Q)$.

    Remember that \insist{each variable in a substitution must be unique}! For example, we have $A \conj B \nsqsubseteq P \conj P$: we can have $\sigma \defequals \left[P \defequals A\right]$ or $\sigma \defequals \left[P \defequals B\right]$, but not both at the same time.

    Note carefully that the $\subsumedby$ operator is not \vocab{commutative} (or \vocab{symmetric}): we have $P \conj P \subsumedby A \conj B$, but $A \conj B \nsubsumedby P \conj P$. This is because the action of applying a substitution is not always ``reversible''.

    Also note the use of the $\identical$ operator instead of the $\equ$ operator! For example, $\true \conj \true \nsubsumedby \true$ simply because they are written differently and neither one has any variables, so there is no possible substitution that will make them \vocab{identical}. This is why we say that subsumption is ``stronger'' than equivalence.

  \subsection{Use in algebra}

    At first the concept of subsumption might seem very abstract and hard to connect to your existing knowledge, but you've actually seen it and worked with it before. Subsumption is the basis of algebraic laws.

    In an algebra course long ago, you learned that $x + y = y + x$ is a law of arithmetic, and you learned to use that fact to conclude things like $1 + 2 = 2 + 1$ or $(a \times b^2) + c = c + (a \times b^2)$.

    But wait --- if we can use $x + y = y + x$ to conclude $1 + 2 = 2 + 1$, why \insist{can't} we use $x + y = y + x$ to conclude $1 + 2 = 2 + 2$?

    Because $(1 + 2 = 2 + 1) \subsumedby (x + y = y + x)$, but $(1 + 2 = 2 + 2) \nsubsumedby (x + y = y + x)$. This is the precise rule which specifies when it is valid or invalid to use the law $x + y = y + x$. There's our answer!


\section{Unification}

  In the field of computer science, we have many algorithms for solving ``subsumption problems'', which are more commonly referred to as \vocabs{unification-problem}.

  \subsection{Definition}

    In general, a \vocab{unification-problem} asks: \insist{given} two expressions $e_1$ and $e_2$, \insist{is there any} substitution $\sigma$ where $e_1 \identical \sigma(e_2)$? In other words, a unification problem asks for ``evidence'' showing that $e_1 \subsumedby e_2$, or ``counter-evidence'' showing that $e_1 \nsubsumedby e_2$.

    The ``evidence'' in a ``yes answer'' to a unification problem is called a \vocab{unifying-substitution}. For example, $\left[P \defequals \true,\ Q \defequals \false \disj \true\right]$ is a unifying substitution which shows that $\true \disj \neg (\false \disj \true) \subsumedby P \disj \neg Q$.

  \subsection{Algorithm}

    We will cover at least a sketch of a \vocab{unification-algorithm} in lecture, but the most important thing to know is that there \insist{are} algorithms for solving unification problems of the kind we've seen in these notes.

    In a sense, unification algorithms give us a way to ``teach'' computers how to reason algebraically. This idea is the basis of technologies like typecheckers, computer algebra systems, and relational databases.


\input{include/footer.tex}
\end{document}
