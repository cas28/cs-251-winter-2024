# This build setup is nasty, but I've never seen a LaTeX build setup that isn't...

# This Makefile is written for a bash-like shell environment with a TexLive
# distribution installed. On Windows, it might be easiest to build with WSL.

# Building the output for deployment requires XeTeX, Python 3, and xindy.
# If any of them is installed with a different executable name, or installed
# somewhere that isn't on your system PATH, you can set the path to each
# executable here.
XETEX=xelatex
PYTHON=python3
XINDY=xindy



#########
## all ##
#########

infos := $(wildcard info/*.tex)
hws := $(wildcard hw/*.tex)
exams := $(wildcard exams/*.tex)
notes := $(wildcard notes/[0-9].tex) $(wildcard notes/[0-9][0-9].tex)
notes-code := $(wildcard notes/code[0-9].lagda.tex) $(wildcard notes/code[0-9][0-9].lagda.tex)

all: \
	$(infos:.tex=.pdf.done) \
	$(hws:.tex=.pdf.done) \
	$(exams:.tex=.pdf.done) \
	$(notes:.tex=.pdf.done) $(notes:.tex=.ttl) \
	$(notes-code:.lagda.tex=.tex) $(notes-code:.lagda.tex=.agdacode)

# Allow XeTeX to use files from the include folder.
export TEXINPUTS=include:



##########
## info ##
##########


info/%.pdf info/%.pdf.done: info/%.tex include
# LaTeX needs two compilation runs to get the table of contents right.
# This can leave behind a .pdf file when the first compilation succeeds and the
# second fails, which confuses Make, so the .pdf.done files give it something
# separate to hang a timestamp on when a build actually succeeds.
	$(XETEX) -interaction=nonstopmode --output-directory=info "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=info "$<"
	touch "$@" # create .pdf.done file if that was the target



##############
## homework ##
##############


hw/%.pdf hw/%.pdf.done: hw/%.tex include include/theories
	$(XETEX) -interaction=nonstopmode --output-directory=hw "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=hw "$<"
	touch "$@"



###########
## exams ##
###########


exams/%.pdf exams/%.pdf.done: exams/%.tex include include/theories
	$(XETEX) -interaction=nonstopmode --output-directory=exams "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=exams "$<"
	touch "$@"



###########
## notes ##
###########


notes/%.ttl: notes/%.tex notes/gettitle.sh
# gettitle.sh extracts the title of each LaTeX document and stores it in a .ttl
# file so that the deployment script can use it to name things later.
	echo -n "$(shell notes/gettitle.sh "$<")"> $(basename $<).ttl

notes/%.pdf notes/%.pdf.done: notes/%.tex include include/theories
# vocab.py generates glossary entries in a .glt file for each vocabulary term
# used in the corresopnding .tex (carefully ignoring any manually-defined
# glossary entries). Each .glt file is manually included in the corresponding
# .tex with an \input command in the preamble.
	$(PYTHON) include/vocab.py "$<" > "$(basename $<).glt"

# For a specific target .tex file, build the corresponding .pdf file.
#
# As far as I can tell this is the recommended way to accomplish a document
# with a table of contents (TOC) and an auto-generated glossary:
#
# 1. The first xelatex run generates $f.glo, some kind of glossary reference
# database, and outputs a PDF that's missing all of the TOC, the glossary, and
# the vocabulary terms.
# 2. The makeglossaries call uses $f.glo to generate $f.gls, the glossary style
# definition.
# 3. The second xelatex call uses $f.glo and $f.gls to output a PDF that has
# the glossary and the vocabulary terms, and the TOC renders partially, but the
# TOC is missing a link to the glossary.
# 4. The third xelatex call finishes rendering the TOC including a
# link to the glossary.
#
# This script could do some clever checking to avoid repeating work if the TOC
# and/or glossary are already up-to-date, but that sounds like a pain to get
# right.
	$(XETEX) -interaction=nonstopmode --output-directory=notes "$<"
	makeglossaries -x $(XINDY) -s "$(basename $<).xdy" "$(basename $<)"
	$(XETEX) -interaction=nonstopmode --output-directory=notes "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=notes "$<"
	touch "$@" # create .pdf.done file if that was the target



# LaTeX is so messy!
.PHONY: clean clean-info clean-hw clean-notes
clean: clean-info clean-hw clean-notes

clean-info:
	cd info; \
	rm -f *.aux *.glg *.glo *.gls *.log *.out *.pdf *.ptb *.toc *.xdy; \
	rm -f *.done

clean-hw:
	cd hw; \
	rm -f *.aux *.glg *.glo *.gls *.log *.out *.pdf *.ptb *.toc *.xdy; \
	rm -f *.done

clean-notes:
	cd notes; \
	rm -f *.aux *.glg *.glo *.gls *.glt *.log *.out *.pdf *.ptb *.toc *.ttl *.xdy; \
	rm -f *.agdacode *.agdai code[0-9].tex code[0-9][0-9].tex; \
	rm -f *.done
